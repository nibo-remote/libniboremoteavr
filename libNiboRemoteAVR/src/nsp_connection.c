/*  BSD-License:

 Copyright (c) 2013 by Nils Springob, nicai-systems, Germany
 Copyright (c) 2013 by Tobias Ilte, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*!
 @file		nsp_connection.c
 @author	Nils Springob
 @author	Tobias Ilte
 @brief		This file handles the complete functionality for sending and receiving data via serial ports.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <nibo/niboconfig.h>
#include <nibo/delay.h>
#include <nibo/bot.h>
#include <nibo/floor.h>
#include <nibo/copro.h>
#include <nibo/gfx.h>
#include <nibo/leds.h>
#include <nibo/nds3.h>
#include <nibo/pwm.h>
#include <nibo/iodefs_nibo2.h>
#include <nibo/uart0.h>
#include <nibo/uart1.h>

#include "nsp_parser.h"
#include "nsp_connection.h"

#define READ_REG(x) myregisters.flags[x]&(NSP_FLAG_REQUESTED|NSP_FLAG_REPORTED)
#define WRITE_REG(x) myregisters.flags[x]&NSP_FLAG_SET

static char transmissionBuffer[256]; //buffer for receiving and transmitting data
static uint8_t firstEmptyBufferPos = 0; //stores index of first empty element in transmissionBuffer[]
static uint8_t txMsgLength; // Length of transmitting message

void nsp_update_registers() {
	bot_update();
	copro_update();
	floor_update();

	if (READ_REG(NSPREG_BOTID)) {
		myregisters.values[NSPREG_BOTID] = 0x4e32;   // Nibo2's ID
	}

	if (READ_REG(NSPREG_VERSION)) {
		myregisters.values[NSPREG_VERSION] = 2;
	}

	if (READ_REG(NSPREG_VSUPPLY)) {
		uint32_t supply = bot_supply;
		supply *= 166;
		supply /= 10;
		supply -= 1190;
		myregisters.values[NSPREG_VSUPPLY] = supply; // [mV]
	}

	if (WRITE_REG(NSPREG_LEDSR)) {
		IO_LEDS_RED_PORT = myregisters.values[NSPREG_LEDSR];
	}

	if (WRITE_REG(NSPREG_LEDSG)) {
		IO_LEDS_GREEN_PORT = myregisters.values[NSPREG_LEDSG];
	}

	if (WRITE_REG(NSPREG_LEDSWPWM)) {
		leds_set_headlights(myregisters.values[NSPREG_LEDSWPWM]);
	}

	if ((WRITE_REG(NSPREG_MOTPWML)) || (WRITE_REG(NSPREG_MOTPWMR)) || (WRITE_REG(NSPREG_MOTPIDL))
			|| (WRITE_REG(NSPREG_MOTPIDR)) || (WRITE_REG(NSPREG_MOTMODE))) {
		switch (myregisters.values[NSPREG_MOTMODE]) {
			case 0:
				copro_stopImmediate();
				break;
			case 1:
				copro_stop();
				break;
			case 2:
				copro_setPWM(myregisters.values[NSPREG_MOTPWML], myregisters.values[NSPREG_MOTPWMR]);
				break;
			case 3:
				copro_setSpeed(myregisters.values[NSPREG_MOTPIDL], myregisters.values[NSPREG_MOTPIDR]);
				break;
		}
	}

	if (READ_REG(NSPREG_MOTCURL)) {
		myregisters.values[NSPREG_MOTCURL] = copro_current_l;
	}

	if (READ_REG(NSPREG_MOTCURR)) {
		myregisters.values[NSPREG_MOTCURR] = copro_current_r;
	}

	if (READ_REG(NSPREG_ODOL)) {
		myregisters.values[NSPREG_ODOL] = copro_ticks_l;
	}

	if (READ_REG(NSPREG_ODOR)) {
		myregisters.values[NSPREG_ODOR] = copro_ticks_r;
	}

	if (READ_REG(NSPREG_FLOORL)) {
		myregisters.values[NSPREG_FLOORL] = floor_relative[FLOOR_LEFT];
	}

	if (READ_REG(NSPREG_FLOORR)) {
		myregisters.values[NSPREG_FLOORR] = floor_relative[FLOOR_RIGHT];
	}

	if (READ_REG(NSPREG_LINEL)) {
		myregisters.values[NSPREG_LINEL] = floor_relative[LINE_LEFT];
	}

	if (READ_REG(NSPREG_LINER)) {
		myregisters.values[NSPREG_LINER] = floor_relative[LINE_RIGHT];
	}

	if (READ_REG(NSPREG_DIST_L)) {
		myregisters.values[NSPREG_DIST_L] = (copro_distance[4] / 256);
	}
	if (READ_REG(NSPREG_DIST_FL)) {
		myregisters.values[NSPREG_DIST_FL] = (copro_distance[3] / 256);
	}
	if (READ_REG(NSPREG_DIST_F)) {
		myregisters.values[NSPREG_DIST_F] = (copro_distance[2] / 256);
	}
	if (READ_REG(NSPREG_DIST_FR)) {
		myregisters.values[NSPREG_DIST_FR] = (copro_distance[1] / 256);
	}
	if (READ_REG(NSPREG_DIST_R)) {
		myregisters.values[NSPREG_DIST_R] = (copro_distance[0] / 256);
	}
#if defined(NIBO_USE_NDS3)
	if (READ_REG(NSPREG_DIST_NDS3)) {
		myregisters.values[NSPREG_DIST_NDS3] = nds3_get_dist();
	}
	if (READ_REG(NSPREG_MOVE_NDS3)) {
		myregisters.values[NSPREG_MOVE_NDS3] = nds3_get_pos();
	}
	if (WRITE_REG(NSPREG_MOVE_NDS3)) {
		if (myregisters.values[NSPREG_MOVE_NDS3] < 0) {
			myregisters.values[NSPREG_MOVE_NDS3] = 0;
		} else if (myregisters.values[NSPREG_MOVE_NDS3] > 180) {
			myregisters.values[NSPREG_MOVE_NDS3] = 180;
		}
		nds3_move(myregisters.values[NSPREG_MOVE_NDS3]);
	}
#endif // NIBO_USE_NDS3
}

void nsp_handle_all(uint8_t uart) {
	if (nsp_get_data(uart)) {
		nsp_update_registers();
		nsp_send_data(uart);
	}
}

uint8_t nsp_get_data(uint8_t uart) {
	if (uart == 0) {
		while (!uart0_rxempty()) {
			char c = uart0_getchar();

			// ignore '\r', which is only used by Windows
			if (c != '\r') {
				transmissionBuffer[firstEmptyBufferPos++] = c;
			}

			// end of message
			if (c == '\n') {
				parse_request(transmissionBuffer, firstEmptyBufferPos - 1);
				firstEmptyBufferPos = 0;
				return 1;
			}
		}
	} else if (uart == 1) {
		while (!uart1_rxempty()) {
			char c = uart1_getchar();

			if (c != '\r') {
				transmissionBuffer[firstEmptyBufferPos++] = c;
			}
			if (c == '\n') {
				parse_request(transmissionBuffer, firstEmptyBufferPos - 1);
				firstEmptyBufferPos = 0;
				return 1;
			}
		}
	}
	return 0;
}

void nsp_send_data(uint8_t uart) {
	txMsgLength = build_answer(transmissionBuffer, sizeof(transmissionBuffer));
	while (firstEmptyBufferPos < txMsgLength) {
		if (uart == 0) {
			if (!uart0_txfull()) {
				uart0_putchar(transmissionBuffer[firstEmptyBufferPos++]);
			}
		} else if (uart == 1) {
			if (!uart1_txfull()) {
				uart1_putchar(transmissionBuffer[firstEmptyBufferPos++]);
			}
		}
	}
	firstEmptyBufferPos = 0;
	txMsgLength = 0;
}

// Initialize serial comm.
void nsp_init(uint8_t uart) {
	if (uart == 0) {
		uart0_set_baudrate(9600);
		uart0_enable();
	} else if (uart == 1) {
		uart1_set_baudrate(9600);
		uart1_enable();
	}
}
