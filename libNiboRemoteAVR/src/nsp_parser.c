/*  BSD-License:

 Copyright (c) 2013 by Nils Springob, nicai-systems, Germany
 Copyright (c) 2013 by Tobias Ilte, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 @file		nsp_parser.c
 @author	Nils Springob
 @author	Tobias Ilte
 @brief		This file interprets and handles the Nibo Serial Protocol.
 */

#include "nsp_parser.h"
#include <avr/pgmspace.h>
#include <string.h>
#include <stdlib.h>

struct registers myregisters;

enum {
	NSP_MODE_NONE = 0,
	NSP_MODE_DEC = 1,
	NSP_MODE_HEX = 2,
	NSP_MODE_SEQ = 4,
	NSP_MODE_ERROR = 64,
	NSP_MODE_HELP = 128
};

/* local variables */
static uint8_t error;
static uint8_t request_mode;
static char * data;
static uint16_t length;
static uint8_t sequence;

static void parse_space() {
	while (length) {
		char c = *data;
		if ((c == ' ') || (c == '\t') || (c == ',')) {
			data++;
			length--;
		} else {
			return;
		}
	}
}

static char parse_command() {
	if (length < 3)
		return 0;
	if (strncmp(data, "get", 3) == 0) {
		data += 3;
		length -= 3;
		return '?';
	}
	if (strncmp(data, "set", 3) == 0) {
		data += 3;
		length -= 3;
		return '!';
	}
	if (length < 4)
		return 0;
	if (strncmp(data, "help", 4) == 0) {
		data += 4;
		length -= 4;
		return '^';
	}
	if (length < 6)
		return 0;
	if (strncmp(data, "report", 6) == 0) {
		data += 6;
		length -= 6;
		return '#';
	}
	if (length < 7)
		return 0;
	if (strncmp(data, "request", 7) == 0) {
		data += 7;
		length -= 7;
		return '*';
	}
	if (length < 8)
		return 0;
	if (strncmp(data, "unreport", 8) == 0) {
		data += 8;
		length -= 8;
		return '~';
	}
	return 0;
}

static uint16_t parse_dec() {
	uint16_t result = 0;
	while (length) {
		char c = *data;
		if ((c >= '0') && (c <= '9')) {
			data++;
			length--;
			result = result * 10 + (c - '0');
		} else {
			return result;
		}
	}
	return result;
}

static uint16_t parse_hex() { //parse hex to int
	uint16_t result = 0;
	while (length) {
		char c = *data;
		if ((c >= '0') && (c <= '9')) {
			data++;
			length--;
			result = result * 16 + (c - '0');
		} else if ((c >= 'a') && (c <= 'f')) {
			data++;
			length--;
			result = result * 16 + (c - ('a' - 10)); // uses ascii-values: 'a' - 10 = 87 ---> a->10, b->11...
		} else {
			return result;
		}
	}
	return result;
}

static uint16_t parse_int() {
	uint16_t result = 0;
	uint8_t neg = 0;
	if (length) {
		if (*data == '-') {
			length--;
			data++;
			neg = 1;
		} else if (*data == '+') {
			length--;
			data++;
		}
	}
	if (request_mode & NSP_MODE_DEC) {
		result = parse_dec();
	} else if (request_mode & NSP_MODE_HEX) {
		result = parse_hex();
	}
	return (neg) ? (-result) : (result);
}

static void begin_dec() {
	if (request_mode != NSP_MODE_NONE) {
		error = 1;
	}
	parse_space();
	request_mode = NSP_MODE_DEC;
	char * ptr = data;
	sequence = parse_int();
	if (ptr != data) {
		request_mode |= NSP_MODE_SEQ;
	}
}

static void begin_hex() {
	if (request_mode != NSP_MODE_NONE) {
		error = 1;
	}
	parse_space();
	request_mode = NSP_MODE_HEX;
	char * ptr = data;
	sequence = parse_int();
	if (ptr != data) {
		request_mode |= NSP_MODE_SEQ;
	}
}

static void cmd_set() {
	uint8_t id;
	uint16_t value;
	parse_space();
	id = parse_int();
	parse_space();
	value = parse_int();
	if (id >= NSP_REGISTERS) {
		error = 1;
		return;
	}
	myregisters.flags[id] |= NSP_FLAG_SET;
	myregisters.values[id] = value;
}

static void cmd_get() {
	uint8_t id;
	parse_space();
	id = parse_int();
	if (id >= NSP_REGISTERS) {
		error = 1;
		return;
	}
	myregisters.flags[id] |= NSP_FLAG_REQUESTED;
}

static void cmd_report() {
	uint8_t id;
	parse_space();
	id = parse_int();
	if (id >= NSP_REGISTERS) {
		error = 1;
		return;
	}
	myregisters.flags[id] |= NSP_FLAG_REPORTED;
}

static void cmd_unreport() {
	uint8_t id;
	parse_space();
	id = parse_int();
	if (id >= NSP_REGISTERS) {
		error = 1;
		return;
	}
	myregisters.flags[id] &= ~NSP_FLAG_REPORTED;
}

static void help() {
	if (request_mode != NSP_MODE_NONE) {
		error = 1;
	} else {
		request_mode = NSP_MODE_HELP;
	}
}

void parse_request(char * rxdata, uint16_t rxlen) {
	data = rxdata;
	length = rxlen;

	while (length) {
		char c = *data;
		if ((c >= 'a') && (c <= 'z')) {
			c = parse_command();
		} else {
			data++;
			length--;
		}

		switch (c) {
			case 0:
				error = 1;
				break;
			case ' ':
			case '\t':
				continue;
			case '*':
				begin_dec();
				break;
			case '$':
				begin_hex();
				break;
			case '!':
				cmd_set();
				break;
			case '?':
				cmd_get();
				break;
			case '#':
				cmd_report();
				break;
			case '~':
				cmd_unreport();
				break;
			case '^':
				help();
				length = 0;
				break;
			default:
				error = 1;
				break;
		}

		if (error) {
			error = 0;
			request_mode = NSP_MODE_ERROR;
			break;
		}
	}
}

static void output_dec(uint16_t value) {
	data += strlen(itoa(value, data, 10));
}

static void output_hex(uint16_t value) {
	data += strlen(itoa(value, data, 16));
	/*
	 uint8_t digits = 4;
	 uint16_t ref = 0xf000;
	 while (digits--) {
	 char c = (value>>12)&0xf;
	 c += (c>=10)?('a'-10):'0';
	 *data ++= c;
	 value <<= 4;
	 }
	 */
}

static void output_int(uint16_t value) {
	if (request_mode & NSP_MODE_DEC) {
		*data++ = ' ';
	}

	if (request_mode & NSP_MODE_DEC) {
		output_dec(value);
	} else if (request_mode & NSP_MODE_HEX) {
		output_hex(value);
	}
}

static void output_command(char c) {
	if (request_mode & NSP_MODE_DEC) {
		switch (c) {
			case ',':
				*data++ = c;
				break;
			case '*':
				strcpy(data, "reply");
				data += 5;
				break;
			case '!':
				strcpy(data, " set");
				data += 4;
				break;
		}
	} else if (request_mode & NSP_MODE_HEX) {
		*data++ = c;
	}
}

uint16_t build_answer(char * txdata, uint16_t txlenmax) {
	data = txdata;
	data[--txlenmax] = 0;
	if (request_mode & NSP_MODE_ERROR) {
		request_mode = 0;
		return strlen(strncpy_P(txdata, PSTR(NSP_ERROR_TEXT), txlenmax));
	}
	if (request_mode & NSP_MODE_HELP) {
		request_mode = 0;
		return 0;
	}
	output_command('*');
	if (request_mode & NSP_MODE_SEQ) {
		output_int(sequence);
	}
	for (uint8_t i = 0; i < NSP_REGISTERS; i++) {
		if (myregisters.flags[i] & (NSP_FLAG_REQUESTED | NSP_FLAG_REPORTED)) {
			myregisters.flags[i] &= ~NSP_FLAG_REQUESTED;
			output_command('!');
			output_int(i);
			output_command(',');
			output_int(myregisters.values[i]);
		}
	}
	if (request_mode & NSP_MODE_DEC) {
		*data++ = '\r';
	}
	*data++ = '\n';
	request_mode = 0;
	return data - txdata;
}

uint8_t nsp_show_help() {
	return request_mode & NSP_MODE_HELP;
}
