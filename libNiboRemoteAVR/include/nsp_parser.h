/*  BSD-License:

 Copyright (c) 2013 by Nils Springob, nicai-systems, Germany
 Copyright (c) 2014 by Tobias Ilte, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*!
 @file		nsp_parser.h
 @authors	Nils Springob
 @authors	Tobias Ilte
 @brief		This file interprets and handles the Nibo Serial Protocol.
 */

#ifndef NSP_PARSER_H_
#define NSP_PARSER_H_

#include <stdint.h>

/// register flag, which indicates, that the value of the associated element is supposed to be set
#define NSP_FLAG_SET		2

/// register flag, which indicates a request for the value of the associated element
#define NSP_FLAG_REQUESTED	4

/// register flag, which indicates a request for automatically sending the value of the associated element at every reply of the robot
#define NSP_FLAG_REPORTED	8

/// size of registers or in other words the number of elements, which can be stored
#define NSP_REGISTERS 32

#define NSP_ERROR_TEXT \
"parsing error! Try help\n"

#define NSP_HELP_TEXT \
"Functions:\n" \
"=========\n" \
"command     short  parameter           function\n" \
"-------     -----  ---------           --------\n" \
"help                                   show this help\n" \
"request            {sequence}          begin decimal request, sequence number is optional\n" \
"            $      {sequence}          begin hexadecimal request, sequence number is optional\n" \
" set        !      {register},{value}  set register with value\n" \
" get        ?      {register}          get register data once\n" \
" report     #      {register}          report register data always\n" \
" unreport   ~      {register}          don't report register data\n" \
"\n"

enum {
	/* System */
	NSPREG_BOTID = 0,
	NSPREG_VERSION = 1,
	NSPREG_VSUPPLY = 2,
	/* LEDs */
	NSPREG_LEDSR = 3,
	NSPREG_LEDSG = 4,
	NSPREG_LEDSWPWM = 5,
	/* Motor */
	NSPREG_MOTMODE = 6,
	NSPREG_MOTPWML = 7,
	NSPREG_MOTPWMR = 8,
	NSPREG_MOTPIDL = 9,
	NSPREG_MOTPIDR = 10,
	NSPREG_MOTCURL = 11,
	NSPREG_MOTCURR = 12,
	/* Odometry */
	NSPREG_ODOL = 13,
	NSPREG_ODOR = 14,
	/* Floor/Line */
	NSPREG_FLOORL = 15,
	NSPREG_FLOORR = 16,
	NSPREG_LINEL = 17,
	NSPREG_LINER = 18,
	/* Distance */
	NSPREG_DIST_L = 19,
	NSPREG_DIST_FL = 20,
	NSPREG_DIST_F = 21,
	NSPREG_DIST_FR = 22,
	NSPREG_DIST_R = 23,
	NSPREG_DIST_NDS3 = 24,
	NSPREG_MOVE_NDS3 = 25
};

#define NSP_REGISTER_TEXT \
  "Registers:\n"\
  "=========\n"\
  " 0: BOT ID\n"\
  " 1: Version\n"\
  " 2: Supply Voltage [mV]\n"\
  " 3: red LEDs\n"\
  " 4: green LEDs\n"\
  " 5: PWM white LEDs\n"\
  " 6: Motor Mode \n"\
  " 7: Motor PWM L \n"\
  " 8: Motor PWM R \n"\
  " 9: Motor PID L \n"\
  "10: Motor PID R \n"\
  "11: Motor current L \n"\
  "12: Motor current R \n"\
  "13: Odometry L \n"\
  "14: Odometry R \n"\
  "15: Floor L\n"\
  "16: Floor R\n"\
  "17: Line L\n"\
  "18: Line R\n"\
  "19: Dist L\n"\
  "20: Dist FL\n"\
  "21: Dist F\n"\
  "22: Dist FR\n"\
  "23: Dist R \n"\
  "24: Dist NDS3 \n"

/**
 * contains 2 arrays:
 *  registers.values contains the sensor data of the robot.
 * Every element of the array represents one sensor/component of the robot.
 * The numbering is compatible with the Nibo Serial Protocol.
 * E.g.: register.values[13] represents the ticks/s of the left motor.
 *
 * registers.flags contains flags, which indicate how data is requested.
 * It has the same numbering as registers.values.
 */
extern struct registers {
	int values[NSP_REGISTERS];
	uint8_t flags[NSP_REGISTERS];
} myregisters;

void parse_request(char * rxdata, uint16_t rxlen);
uint16_t build_answer(char * txdata, uint16_t txlenmax);

#endif /* NSP_PARSER_H_ */
