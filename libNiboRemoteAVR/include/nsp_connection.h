/*  BSD-License:

 Copyright (c) 2013 by Nils Springob, nicai-systems, Germany
 Copyright (c) 2013 by Tobias Ilte, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*!
 * @file	nsp_connection.h
 * @authors	Nils Springob
 * @authors	Tobias Ilte
 * @brief	Functions for using the Nibo Serial Protocol in a high-level manner.
 * Functions for sending and receiving data via the serial ports Uart0 and Uart1 using the Nibo Serial Protocol.
 * Before using the functions, serial_init(0) or serial_init(1) must be called.
 */

#ifndef NSP_CONNECTION_H_
#define NSP_CONNECTION_H_

/**
 * Initalizes the serial interface.
 * @param uart defines the used serial-port(Uart0/Uart1)
 */
void nsp_init(uint8_t uart);

/**
 * Updates Sensors and writes requested data into register.
 */
void nsp_update_registers();

/**
 * Combination of nsp_get_data(), nsp_update_registers() and nsp_send_data().
 * Reads data from the serial port, updates registers and sends answer via same serial port.
 * @param uart defines the used serial-port(Uart0/Uart1)
 */
void nsp_handle_all(uint8_t uart);

/**
 * Reads data from the serial port.
 * @param uart defines the used serial-port(Uart0/Uart1)
 * @return 1, if a complete message was successfully received, otherwise 0
 */
uint8_t nsp_get_data(uint8_t uart);

/**
 * Sends data via serial port.
 * @param uart defines the used serial-port(Uart0/Uart1)
 */
void nsp_send_data(uint8_t uart);

#endif /* NSP_CONNECTION_H_ */
